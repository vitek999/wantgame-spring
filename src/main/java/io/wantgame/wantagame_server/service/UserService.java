package io.wantgame.wantagame_server.service;

import io.wantgame.wantagame_server.dao.UserDao;
import io.wantgame.wantagame_server.dto.UserDto;
import io.wantgame.wantagame_server.exceptions.UserNotFoundException;
import io.wantgame.wantagame_server.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private UserDao userDao;

    @Autowired
    public UserService(UserDao userDao) {
        this.userDao = userDao;
    }

    public UserDto login() {
        return getAuthUserCredentials().toDto();
    }

    public User getAuthUserCredentials() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userDao.findByPhone(userDetails.getUsername()).orElseThrow(() ->
                new UserNotFoundException("User with phone:" + userDetails.getUsername() + " not found"));
    }
}
