package io.wantgame.wantagame_server.dto;

import lombok.Data;

@Data
public class RoleDto {
    private Long id;

    private String name;

    private String description;
}
