package io.wantgame.wantagame_server.dao;

import io.wantgame.wantagame_server.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserDao extends CrudRepository<User, Long> {

    Optional<User> findByPhone(String phone);

}
