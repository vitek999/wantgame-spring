package io.wantgame.wantagame_server.controller;

import io.wantgame.wantagame_server.exceptions.UserNotFoundException;
import io.wantgame.wantagame_server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("login")
    public ResponseEntity login() {
        try {
            return new ResponseEntity<>(userService.login(), HttpStatus.OK);
        } catch (UserNotFoundException e) {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
    }
}
