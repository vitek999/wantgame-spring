package io.wantgame.wantagame_server.model;

import io.wantgame.wantagame_server.dto.UserDto;
import lombok.Data;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="Phone")
    private String phone;

    @Column(name = "Password")
    private String password;

    @Column(name = "Firstname")
    private String firstName;

    @Column(name = "Lastname")
    private String lastName;

    @Column(name = "Aboutme")
    private String aboutMe;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(name = "Users_Id", referencedColumnName = "Id"),
            inverseJoinColumns = @JoinColumn(name = "Roles_id", referencedColumnName = "Id")
    )
    private List<Role> roles;

    public UserDto toDto() {
        UserDto dto = new UserDto();
        dto.setId(id);
        dto.setPhone(phone);
        dto.setPassword(password);
        dto.setFirstName(firstName);
        dto.setLastName(lastName);
        dto.setAboutMe(aboutMe);
        dto.setRoles(roles.stream().map(Role::toDto).collect(Collectors.toList()));
        return dto;
    }
}
