package io.wantgame.wantagame_server.model;

import io.wantgame.wantagame_server.dto.RoleDto;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "roles")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "Name")
    private String name;

    @Column(name = "Description")
    private String description;

    public RoleDto toDto() {
        RoleDto roleDto = new RoleDto();
        roleDto.setId(id);
        roleDto.setName(name);
        roleDto.setDescription(description);
        return roleDto;
    }
}
